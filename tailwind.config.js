/** @type {import('tailwindcss').Config} */
export default {
  darkMode : 'class',
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx,vue}",
    "./assets/styles/main.css"
  ],
  theme: {
    extend: {
      colors : {
        primary : '#426bfd',
        primaryDarker : '#3b60e4',
        secondary : '#fec121',
        text : {
          primary : {light : '#000000' , dark : '#ffffff'},
          secondary : {light : '#909090' , dark : '#b0b0b0'}
        },
        background : {
          primary : {light : '#ffffff' , dark : '#000000'},
          secondary : {light : '#f1f1f1' , dark : '#191919'},
          tertiary : {light : '#e0e0e0' , dark : '#303030'}
        },
        border : {
          primary : {light : '#e9e9e9' , dark : '#404040'}
        }
      },
      fontSize : {
        12 : '0.75rem',
        14 : '0.875rem',
        16 : '1rem',
        20 : '1.25rem',
        24 : '1.5rem' ,
        28 : '1.75rem',
        32 : '2rem'
      },
      fontFamily: {
        inter : ["inter"],
      },
      fontWeight: {
        ultralight: '50',
        light: '100',
        regular: '150',
        medium: '200',
        semibold: '250',
        bold: '300',
        extrabold: '350',
        black: '400',
        heavy: '450',
      },
      borderRadius : {
        xs : '0.313rem',
        sm : '0.625rem',
        md : '0.75rem',
        lg : '0.875rem',
        xl : '1rem',
      }
    },
  },
  plugins: [],
}

