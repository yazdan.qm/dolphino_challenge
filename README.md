# dolphino_challenge

This is a small online shop that created with Vue.js for its front-end, Tailwind as its CSS framework and Dummyjson for its fake data. You can add, edit or remove products to this shop and change color mode to light or dark mode. This shop is also used Veevalidate for its form's validation and Pinia for its state managing.

## Technologies and libraries

- [Vue.js](https://vuejs.org/)
- [Tailwind](https://tailwindcss.com/)
- [Dummyjson](https://dummyjson.com/)
- [Axios](https://github.com/axios/axios)
- [Pinia](https://pinia.vuejs.org/)
- [Vue3-toastify](https://github.com/jerrywu001/vue3-toastify)
- [Splide](https://splidejs.com/)
- [Veevalidate](https://vee-validate.logaretm.com/)

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
