// imports
import {app} from '@/core/services/app.service';
import { createPinia } from 'pinia'
import router from '@/core/router'

//plugin service provider
import "@/core/plugins/plugin-service-provider";

// styles
import '@/assets/styles/main.css'
import '@/assets/styles/iconfont/flaticon_test.css'
import '@/assets/styles/checkbox.scss'

app.use(createPinia())
app.use(router)
app.mount('#app')
