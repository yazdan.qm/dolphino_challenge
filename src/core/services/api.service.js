// imports
import axios from 'axios'
import { useBaseStore } from '../stores/base'
import router from '../router'

// headers
let headers = {
  accept: `application/json`,
  'Content-Type': `application/json`,
  'X-Requested-With': `XMLHttpRequest`
}

// api instance
const api = axios.create({
  baseURL: 'https://dummyjson.com',
  headers: headers
})

// error handling
const baseStore = useBaseStore()
const error_handler = (errorObject = undefined) => {
  if (errorObject.code === 'ECONNABORTED' || errorObject.code === 'ERR_NETWORK') {
    baseStore.error_code = 503
    baseStore.error_message = '503 | Service Unavailable'
    return router.push({ name: 'errors_view' }).catch((err) => {})
  } else if (errorObject.response.status === 404) {
    baseStore.error_code = 404
    baseStore.error_message = '404 | Not found'
    return router.push({ name: 'errors_view' }).catch((err) => {})
  } else if (errorObject.response.status === 500) {
    baseStore.error_code = 500
    baseStore.error_message = '500 | Internal server error'
    return router.push({ name: 'errors_view' }).catch((err) => {})
  } else {
    baseStore.error_code = 501
    baseStore.error_message = 'Something went wrong'
    return router.push({ name: 'errors_view' }).catch((err) => {})
  }
}

export { api, error_handler }
