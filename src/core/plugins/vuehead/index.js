import { app } from '@/core/services/app.service'
import { createHead } from '@vueuse/head'
const head = createHead()
app.use( head );