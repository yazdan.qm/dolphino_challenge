// imports
import { app } from '@/core/services/app.service'
import { defineRule, configure } from 'vee-validate'
import * as rules from '@vee-validate/rules'
import { Form, Field, ErrorMessage } from 'vee-validate'
import { localize, setLocale } from '@vee-validate/i18n'
import en from '@vee-validate/i18n/dist/locale/en.json'

// custom messages - for future
let customFaMessages = {}
let customizeMessages = () => {
  Object.keys(en.messages).forEach((message) => {
    if (message === 'email') {
      customFaMessages[message] = 'Email is not valid!'
    } else if (message === 'regex') {
      customFaMessages[message] = ' {field} is not valid!'
    } else {
      customFaMessages[message] = en.messages[message]
    }
  })
}
customizeMessages()

// define rules
Object.keys(rules)
  .filter((k) => k !== 'default')
  .forEach((rule) => {
    defineRule(rule, rules[rule])
  })

// configuration
configure({
  generateMessage: localize({
    en: {
      messages: {
        ...customFaMessages
      }
    }
  }),
  validateOnChange: true,
  validateOnInput: true,
  validateOnBlur: false
})

// set default locale
setLocale('en')

// add to app
app.component('VeeForm', Form)
app.component('VeeField', Field)
app.component('VeeErrorMessage', ErrorMessage)
