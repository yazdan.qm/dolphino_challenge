import { app } from '@/core/services/app.service'
import Vue3Toastify from 'vue3-toastify';
import 'vue3-toastify/dist/index.css';

app.use(Vue3Toastify, {
    autoClose: 3000,
    theme : localStorage.getItem('theme') ? localStorage.getItem('theme') : 'dark',
    transition: "flip",
    position: "top-center",
    dangerouslyHTMLString : true,
});
