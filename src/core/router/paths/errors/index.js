// import error from "@/core/middlewares/error";
export default [
    {
        path: "/error",
        name: "errors",
        component: () => import("@/layouts/ErrorsLayout.vue"),
        children: [
            {
                path: "/:pathMatch(.*)*",
                name: "errors_view",
                component: () =>import ("@/views/ErrorView.vue"),
                meta: {
                    title : "404",
                    code: 404
                }
            },
        ]
    }
]