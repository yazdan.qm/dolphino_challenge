import home from "@/core/router/paths/general/home";
import contact from "@/core/router/paths/general/contact"
import products from "@/core/router/paths/general/products"
import privacy from "@/core/router/paths/general/privacy"


export default [
    {
        path: "/",
        component: () => import("@/layouts/MasterLayout.vue"),
        children: [
            ...home,
            ...contact,
            ...products,
            ...privacy
        ]
    }
]