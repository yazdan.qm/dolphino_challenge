export default [
  {
    path: '/products/add',
    name: 'add-product',
    component: () => import('@/views/AddProductView.vue')
  },
  {
    path: '/products/:id/edit',
    name: 'edit-product',
    component: () => import('@/views/EditProductView.vue')
  },
  {
    path: '/products/:id',
    name: 'single-product',
    component: () => import('@/views/ProductView.vue')
  },
  {
    path: '/products',
    redirect: "/"
  }
]
