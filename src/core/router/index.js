import { createRouter, createWebHistory } from 'vue-router'
import general from '@/core/router/paths/general'
import errors from "@/core/router/paths/errors"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    ...general,
    ...errors
  ],
  scrollBehavior() {
    return { top: 0 }
  }
})

export default router
