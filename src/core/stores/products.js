import { ref } from 'vue'
import { api, error_handler } from '@/core/services/api.service'
import { defineStore } from 'pinia'

export const useProductsStore = defineStore('products', () => {
  //data
  const loading = ref(false)
  const list = ref([])

  // methods
  const getAllProducts = async () => {
    loading.value = true
     await api
      .get('/products')
      .then((res) => {
        list.value = res.data.products
      })
      .catch((error) => {
        error_handler(error)
      })
      .finally(() => {
        loading.value = false
      })
  }

  return { getAllProducts, loading , list }
})
