import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useBaseStore = defineStore('base', () => {
  //data
  const theme = ref(localStorage.getItem('theme') ? localStorage.getItem('theme') : 'dark')
  const error_code = null
  const error_message = null

  // methods
  const changeThemeColorMode = (darkModeEnabled) => {
    if (darkModeEnabled) {
      localStorage.setItem('theme', 'dark')
      if (!document.body.classList.contains('dark')) document.body.classList.add('dark')
      theme.value = 'dark'
    } else {
      localStorage.setItem('theme', 'light')
      document.body.classList.remove('dark')
      theme.value = 'light'
    }
  }

  return { theme, changeThemeColorMode, error_code, error_message }
})
