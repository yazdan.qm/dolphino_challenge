import { ref } from 'vue'
import { api, error_handler } from '@/core/services/api.service'
import { defineStore } from 'pinia'

export const useEditProductStore = defineStore('edit_product', () => {
  //data
  const loading = ref(false)
  const product = ref({})

  // methods
  const getProduct = async (id) => {
    loading.value = true
    product.value = {}
    await api
      .get(`/products/${id}`)
      .then((res) => {
        product.value = res.data
      })
      .catch((error) => {
        error_handler(error)
      })
      .finally(() => {
        loading.value = false
      })
  }

  return { getProduct, loading, product }
})
